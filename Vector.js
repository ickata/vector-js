export class Vector {
    #values = null;

    constructor(...values) {
        if (values.length !== 3 || !values.every(v => !isNaN(Number(v)))) {
            throw new Error('Not a valid Vector');
        }
        this.#values = values;
    }

    #validate(vector) {
        if (!vector || !(vector instanceof Vector)) {
            throw new Error('cross method requires a valid Vector');
        }
    }

    toArray() {
        return [...this.#values];
    }

    cross(vector) {
        this.#validate(vector);
        const [a1, a2, a3] = this.#values;
        const [b1, b2, b3] = vector.toArray();
        return new Vector(
            a2 * b3 - a3 * b2,
            a3 * b1 - a1 * b3,
            a1 * b2 - a2 * b1
        );
    }

    dot(vector) {
        this.#validate(vector);
        const [a1, a2, a3] = this.#values;
        const [b1, b2, b3] = vector.toArray();
        return a1 * b1 + a2 * b2 + a3 * b3;
    }
}
