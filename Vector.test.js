import { Vector } from './Vector';

describe('Vector', () => {
    describe('constructor', () => {
        it('should throw on invalid construct', () => {
            expect(() => new Vector()).toThrow();
            expect(() => new Vector(1)).toThrow();
            expect(() => new Vector(1, 1)).toThrow();
            expect(() => new Vector(1, 1, 'a')).toThrow();
        });

        it('constructs a Vector', () => {
            expect(new Vector(1, 1, 1)).toEqual({});
        });
    });

    describe('toArray', () => {
        it('returns array', () => {
            expect(new Vector(1, 1, 1).toArray()).toEqual([1, 1, 1]);
        });
    });

    describe('cross', () => {
        it('accepts only valid Vector', () => {
            expect(() => new Vector(1, 1, 1).cross()).toThrow();
            expect(() => new Vector(1, 1, 1).cross({})).toThrow();
            expect(() => new Vector(1, 1, 1).cross(new Vector())).toThrow();
            expect(() => new Vector(1, 1, 1).cross(new Vector(1, 1))).toThrow();
            expect(() =>
                new Vector(1, 1, 1).cross(new Vector(1, 1, 'a'))
            ).toThrow();
        });

        it('returns cross', () => {
            expect(
                new Vector(1, 1, 1).cross(new Vector(2, 2, 2)).toArray()
            ).toEqual([0, 0, 0]);
        });
    });

    describe('dot', () => {
        it('accepts only valid Vector', () => {
            expect(() => new Vector(1, 1, 1).cross()).toThrow();
            expect(() => new Vector(1, 1, 1).cross({})).toThrow();
            expect(() => new Vector(1, 1, 1).cross(new Vector())).toThrow();
            expect(() => new Vector(1, 1, 1).cross(new Vector(1, 1))).toThrow();
            expect(() =>
                new Vector(1, 1, 1).cross(new Vector(1, 1, 'a'))
            ).toThrow();
        });

        it('returns dot', () => {
            expect(new Vector(1, 1, 1).dot(new Vector(2, 2, 2))).toEqual(6);
        });
    });
});
