# class Vector

## Overview

Produces a new Vector instance

## Requirements

 - NodeJS v14 and above
 - `yarn` (`npm` also works)

## Setup

`yarn install`

## Run unit tests

`yarn test`

## Interface

### Constructor

```js
import { Vector } from './Vector';
const vector = new Vector(1, 2, 3);
```

### `toArray`

Converts the `Vector` into `Array`.

#### Returns

`Array`

### `cross`

Returns cross product.

#### Parameters

1. `Vector`

#### Returns

`Vector`

### `dot`

Returns dot product.

#### Parameters

1. `Vector`

#### Returns

`Number`
